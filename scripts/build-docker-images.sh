#!/usr/bin/env sh

DOCKER_REPOSITORY=${1:-testing}
IMAGE_NAME=${2:-testing}
IMAGE_VERSION=${3:-unknown}

build() {
    input=$1
    arch=$2
    arg1=$3
    tag="${DOCKER_REPOSITORY}/${arch}/${IMAGE_NAME}"
    dockerfile_path="${arch}.Dockerfile"

    sed "s/i386/$arch/;" ${input} > ${dockerfile_path}
    ls -l ${dockerfile_path}
    if [ "$arg1" = "cross" ]; then
        dockerfile_path_tmp="${dockerfile_path}.tmp"
        cp ${dockerfile_path} ${dockerfile_path_tmp}
        cat ${dockerfile_path_tmp} | sed "s/^#//;" > ${dockerfile_path}
        rm ${dockerfile_path_tmp}
    fi

    docker build -t "${tag}:${IMAGE_VERSION}" -f ${dockerfile_path} . \
        && docker push "${tag}:${IMAGE_VERSION}" \
        && rm ${dockerfile_path}
}

build "Dockerfile" "i386" \
    && build "Dockerfile" "amd64" \
    && build "Dockerfile" "aarch64" "cross" \
    && build "Dockerfile" "armhf" "cross"
