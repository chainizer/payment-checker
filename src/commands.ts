import {Metadata, registerTask, taskLogger, TaskType} from '@chainizer/support-executor';
import {dispatchTask} from '@chainizer/support-cnq';
import {
    CHECK_PAYMENT,
    CheckPaymentCmd,
    FETCH_PAYMENT,
    FetchPaymentQry,
    FetchPaymentQryRes,
    GET_BALANCE,
    GetBalanceQry,
    GetBalanceQryRes,
    INSERT_PAYMENT,
    InsertEventCmd
} from '@chainizer/payment-api';
import BigNumber from 'bignumber.js';
import * as moment from 'moment';
import * as request from 'request-promise-native';
import {v4 as uuid} from 'uuid';

async function getBalance(transactionId: string, context: CheckPaymentCmd): Promise<BigNumber> {
    const payload: GetBalanceQry = {
        address: context.address
    };
    const metadata: Metadata = {
        messageId: uuid(),
        transactionId
    };
    const balanceRes = <GetBalanceQryRes> await dispatchTask(context.coin, TaskType.query, GET_BALANCE, payload, metadata);
    return new BigNumber(balanceRes.balance);
}

async function getCallbackUrl(transactionId: string, context: CheckPaymentCmd): Promise<any> {
    const payload: FetchPaymentQry = {
        coin: context.coin,
        address: context.address
    };
    const metadata: Metadata = {
        transactionId,
        challenge: context.challenge,
        messageId: uuid()
    };
    const paymentRes = <FetchPaymentQryRes> await dispatchTask('repository', TaskType.query, FETCH_PAYMENT, payload, metadata);
    return paymentRes.callbackUrl;
}

async function logEvent(transactionId: string, context: CheckPaymentCmd, error?: string) {
    const payload: InsertEventCmd = {
        coin: context.coin,
        address: context.address,
        type: 'notification',
        attemptedAt: moment().toISOString(),
        successful: !!error,
        error
    };
    const metadata: Metadata = {
        transactionId,
        challenge: context.challenge,
        messageId: uuid()
    };
    await dispatchTask('repository', TaskType.command, INSERT_PAYMENT, payload, metadata);
}

async function queueTaskAgain(payload: CheckPaymentCmd) {
    const metadata = {
        messageId: uuid()
    };
    await dispatchTask('checker', TaskType.command, CHECK_PAYMENT, payload, metadata, false);
}

registerTask(TaskType.command, CHECK_PAYMENT, async (payload: CheckPaymentCmd, metadata: Metadata): Promise<any> => {
    const l = taskLogger(metadata);
    let success = true;
    let queued = false;

    // fetch account balance
    // if balance > 0
    //   call callback
    //   store callback result
    //   if result OK
    //     exit
    // if command TTL < 3 day
    //   dispatch command (delayed 1 hour)

    const balance = await getBalance(metadata.messageId, payload);

    if (balance.gt(0)) {
        const callbackUrl = await getCallbackUrl(metadata.messageId, payload);
        try {
            l.debug('call [%s]', callbackUrl);
            await request.get(callbackUrl);
            await logEvent(metadata.messageId, payload);
        } catch (e) {
            success = false;
            l.debug('[%s] called failed [%s/%s]', callbackUrl, e.name, e.message);
            await logEvent(metadata.messageId, payload, `${e.name}/${e.message}`);
        }
    }

    const now = moment();
    const timeToLive = moment(payload.timeToLive);
    if (timeToLive.isAfter(now)) {
        await queueTaskAgain(payload);
        queued = true;
    }

    return {success, queued};
});