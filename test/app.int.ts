import {expect} from 'chai';
import 'mocha';
import {getHooks, getServer} from '@chainizer/support-app';
import {dispatchTask, getCnqAmqp} from '@chainizer/support-cnq';
import {TaskType} from '@chainizer/support-executor';
import {CHECK_PAYMENT} from '@chainizer/payment-api';
import {config} from '@chainizer/support-config';
import '../src/main';

const wait = (ms) => (new Promise(resolve => setTimeout(resolve, ms)));

config().set('cnq.producer.amqp.command.checker.routingKey', 'command');

describe('app', () => {

    it('should check health and stop the app', async () => {
        await wait(100);

        let healthError;
        try {
            getHooks().health.forEach(async hook => await hook());
        } catch (e) {
            healthError = e;
        }


        const amqp = await getCnqAmqp();
        await amqp.channel.checkExchange('payment.checker');
        await amqp.channel.checkQueue('payment.checker.command_backlog');
        await amqp.channel.checkQueue('payment.checker.command_delayed');
        await amqp.channel.checkQueue('payment.checker.command_invalid');

        try {
            const r = await dispatchTask('checker', TaskType.command, CHECK_PAYMENT, {}, {});
            expect(r).to.not.exist;
        } catch (e) {
            expect(e).to.exist;
        }

        let shutdownError;
        try {
            getHooks().shutdown.forEach(async hook => await hook());
        } catch (e) {
            shutdownError = e;
        }

        getServer().close();
        expect(healthError, 'health hooks failed').to.not.exist;
        expect(shutdownError, 'shutdown hooks failed').to.not.exist;
    });

});

