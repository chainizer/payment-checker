import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {FakeCnq} from '@chainizer/support-cnq-test';
import {expect} from 'chai';
import 'mocha';
import * as moment from 'moment';
import '../src/commands';
import {CHECK_PAYMENT, CheckPaymentCmd, CheckPaymentCmdRes, Coin} from '@chainizer/payment-api';

describe('check-payment', () => {
    const f = new FakeCnq();

    beforeEach(async () => {
        f.reset();
    });

    it('should only call and log success event', async () => {
        f.handle(
            0,
            (exchange, routingKey) => {
                expect(exchange).to.be.eq('payment.ethereum');
                expect(routingKey).to.be.eq('query');
            },
            () => new Buffer(JSON.stringify({balance: '1'}))
        );
        f.handle(
            1,
            (exchange, routingKey) => {
                expect(exchange).to.be.eq('payment.repository');
                expect(routingKey).to.be.eq('query');
            },
            () => new Buffer(JSON.stringify({callbackUrl: 'https://payment.chainizer.com'}))
        );
        f.handle(
            2,
            (exchange, routingKey) => {
                expect(exchange).to.be.eq('payment.repository');
                expect(routingKey).to.be.eq('command');
            }
        );

        const payload: CheckPaymentCmd = {
            address: 'address',
            coin: Coin.ethereum,
            challenge: 'challenge',
            timeToLive: moment().toISOString()
        };
        const metadata: Metadata = {
            messageId: 'test'
        };
        const r = <CheckPaymentCmdRes> await executeTask(TaskType.command, CHECK_PAYMENT, payload, metadata);

        expect(r).to.exist;
        expect(r).to.have.property('success', true);
        expect(r).to.have.property('queued', false);
    });

    it('should only call and log failed event', async () => {
        f.handle(
            0,
            (exchange, routingKey) => {
                expect(exchange).to.be.eq('payment.ethereum');
                expect(routingKey).to.be.eq('query');
            },
            () => new Buffer(JSON.stringify({balance: '1'}))
        );
        f.handle(
            1,
            (exchange, routingKey) => {
                expect(exchange).to.be.eq('payment.repository');
                expect(routingKey).to.be.eq('query');
            },
            () => new Buffer(JSON.stringify({callbackUrl: 'https://payment.chainizer.com/404'}))
        );
        f.handle(
            2,
            (exchange, routingKey) => {
                expect(exchange).to.be.eq('payment.repository');
                expect(routingKey).to.be.eq('command');
            }
        );

        const payload: CheckPaymentCmd = {
            address: 'address',
            coin: Coin.ethereum,
            challenge: 'challenge',
            timeToLive: moment().toISOString()
        };
        const metadata: Metadata = {
            messageId: 'test'
        };
        const r = <CheckPaymentCmdRes> await executeTask(TaskType.command, CHECK_PAYMENT, payload, metadata);

        expect(r).to.exist;
        expect(r).to.have.property('success', false);
        expect(r).to.have.property('queued', false);
    });

    it('should not call and queue', async () => {
        f.handle(
            0,
            (exchange, routingKey) => {
                expect(exchange).to.be.eq('payment.ethereum');
                expect(routingKey).to.be.eq('query');
            },
            () => new Buffer(JSON.stringify({balance: '0'}))
        );

        const payload: CheckPaymentCmd = {
            address: 'address',
            coin: Coin.ethereum,
            challenge: 'challenge',
            timeToLive: moment().add(1, 'day').toISOString()
        };
        const metadata: Metadata = {
            messageId: 'test'
        };
        const r = <CheckPaymentCmdRes> await executeTask(TaskType.command, CHECK_PAYMENT, payload, metadata);

        expect(r).to.exist;
        expect(r).to.have.property('success', true);
        expect(r).to.have.property('queued', true);
    });
});

